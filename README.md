# Rand (Random)

This package is an extension to `math/rand`, providing cryptographically secure random numbers.

The interface of this package adopts `math/rand/v2` and is forward compatible to Go 1.12

This package cannot directly replace `math/rand` or `math/rand/v2`, you need to write new code to use this library

# RNG (Random Number Generator)

This package uses `math/rand` as the RNG.

In Go 1.22 and later versions, `math/rand/v2` is used as the RNG.
