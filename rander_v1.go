// +build !go1.22
//go:build !go1.22

package rand

import (
	crand "crypto/rand"
	"io"
	"math"
	"math/big"
	"math/bits"
	"math/rand"
	"time"
)

const is32bit = ^uint(0)>>32 == 0

var maxInt64 = big.NewInt(math.MaxInt64)

var _ Rander = &Rand{}

type Rand struct {
	rander *rand.Rand
}

func New() Rander {
	seed := time.Now().Unix()

	return &Rand{
		rander: rand.New(rand.NewSource(seed)),
	}
}

func NewCrypto(r io.Reader) (Rander, error) {
	seed, err := crand.Int(r, maxInt64)
	if err != nil {
		return nil, err
	}

	return &Rand{
		rander: rand.New(rand.NewSource(seed.Int64())),
	}, nil
}

func mustCryptoSeed() int64 {
	seed, err := crand.Int(crand.Reader, maxInt64)
	if err != nil {
		return time.Now().UnixNano()
	}

	return seed.Int64()
}

func MustNewCrypto() Rander {
	seed := mustCryptoSeed()

	return &Rand{
		rander: rand.New(rand.NewSource(seed)),
	}
}

func (r *Rand) Int() int {
	return r.rander.Int()
}

func (r *Rand) IntN(n int) int {
	return r.rander.Intn(n)
}

func (r *Rand) Int32() int32 {
	return r.rander.Int31()
}

func (r *Rand) Int32N(n int32) int32 {
	return r.rander.Int31n(n)
}

func (r *Rand) Int64() int64 {
	return r.rander.Int63()
}

func (r *Rand) Int64N(n int64) int64 {
	return r.rander.Int63n(n)
}

func (r *Rand) UintN(n uint) uint {
	return uint(r.Uint64N(uint64(n)))
}

func (r *Rand) Uint32() uint32 {
	return r.rander.Uint32()
}

func (r *Rand) Uint32N(n uint32) uint32 {
	if n&(n-1) == 0 { // n is power of two, can mask
		return uint32(r.Uint64()) & (n - 1)
	}
	// On 64-bit systems we still use the uint64 code below because
	// the probability of a random uint64 lo being < a uint32 n is near zero,
	// meaning the unbiasing loop almost never runs.
	// On 32-bit systems, here we need to implement that same logic in 32-bit math,
	// both to preserve the exact output sequence observed on 64-bit machines
	// and to preserve the optimization that the unbiasing loop almost never runs.
	//
	// We want to compute
	// 	hi, lo := bits.Mul64(r.Uint64(), n)
	// In terms of 32-bit halves, this is:
	// 	x1:x0 := r.Uint64()
	// 	0:hi, lo1:lo0 := bits.Mul64(x1:x0, 0:n)
	// Writing out the multiplication in terms of bits.Mul32 allows
	// using direct hardware instructions and avoiding
	// the computations involving these zeros.
	x := r.Uint64()
	lo1a, lo0 := bits.Mul32(uint32(x), n)
	hi, lo1b := bits.Mul32(uint32(x>>32), n)
	lo1, c := bits.Add32(lo1a, lo1b, 0)
	hi += c
	if lo1 == 0 && lo0 < uint32(n) {
		n64 := uint64(n)
		thresh := uint32(-n64 % n64)
		for lo1 == 0 && lo0 < thresh {
			x := r.Uint64()
			lo1a, lo0 = bits.Mul32(uint32(x), n)
			hi, lo1b = bits.Mul32(uint32(x>>32), n)
			lo1, c = bits.Add32(lo1a, lo1b, 0)
			hi += c
		}
	}
	return hi
}

func (r *Rand) Uint64() uint64 {
	return r.rander.Uint64()
}

func (r *Rand) Uint64N(n uint64) uint64 {
	if is32bit && uint64(uint32(n)) == n {
		return uint64(r.Uint32N(uint32(n)))
	}
	if n&(n-1) == 0 { // n is power of two, can mask
		return r.Uint64() & (n - 1)
	}

	// Suppose we have a uint64 x uniform in the range [0,2⁶⁴)
	// and want to reduce it to the range [0,n) preserving exact uniformity.
	// We can simulate a scaling arbitrary precision x * (n/2⁶⁴) by
	// the high bits of a double-width multiply of x*n, meaning (x*n)/2⁶⁴.
	// Since there are 2⁶⁴ possible inputs x and only n possible outputs,
	// the output is necessarily biased if n does not divide 2⁶⁴.
	// In general (x*n)/2⁶⁴ = k for x*n in [k*2⁶⁴,(k+1)*2⁶⁴).
	// There are either floor(2⁶⁴/n) or ceil(2⁶⁴/n) possible products
	// in that range, depending on k.
	// But suppose we reject the sample and try again when
	// x*n is in [k*2⁶⁴, k*2⁶⁴+(2⁶⁴%n)), meaning rejecting fewer than n possible
	// outcomes out of the 2⁶⁴.
	// Now there are exactly floor(2⁶⁴/n) possible ways to produce
	// each output value k, so we've restored uniformity.
	// To get valid uint64 math, 2⁶⁴ % n = (2⁶⁴ - n) % n = -n % n,
	// so the direct implementation of this algorithm would be:
	//
	//	hi, lo := bits.Mul64(r.Uint64(), n)
	//	thresh := -n % n
	//	for lo < thresh {
	//		hi, lo = bits.Mul64(r.Uint64(), n)
	//	}
	//
	// That still leaves an expensive 64-bit division that we would rather avoid.
	// We know that thresh < n, and n is usually much less than 2⁶⁴, so we can
	// avoid the last four lines unless lo < n.
	//
	// See also:
	// https://lemire.me/blog/2016/06/27/a-fast-alternative-to-the-modulo-reduction
	// https://lemire.me/blog/2016/06/30/fast-random-shuffling
	hi, lo := bits.Mul64(r.Uint64(), n)
	if lo < n {
		thresh := -n % n
		for lo < thresh {
			hi, lo = bits.Mul64(r.Uint64(), n)
		}
	}
	return hi
}

func (r *Rand) ExpFloat64() float64 {
	return r.rander.ExpFloat64()
}

func (r *Rand) NormFloat64() float64 {
	return r.rander.NormFloat64()
}

func (r *Rand) Float32() float32 {
	return r.rander.Float32()
}

func (r *Rand) Float64() float64 {
	return r.rander.Float64()
}

func (r *Rand) Perm(n int) []int {
	return r.rander.Perm(n)
}

func (r *Rand) Shuffle(n int, swap func(i int, j int)) {
	r.rander.Shuffle(n, swap)
}
