// +build go1.22
//go:build go1.22

package rand

import (
	crand "crypto/rand"
	"io"
	"math/rand/v2"
	"time"
)

var _ Rander = &Rand{}

type Rand struct {
	rander *rand.Rand
}

func New() Rander {
	src := time.Now()
	seed1 := uint64(src.UnixNano())
	seed2 := uint64(src.Unix())

	return &Rand{
		rander: rand.New(rand.NewPCG(seed1, seed2)),
	}
}

func NewCrypto(r io.Reader) (Rander, error) {
	var seed [32]byte
	if _, err := io.ReadFull(r, seed[:]); err != nil {
		return nil, err
	}

	return &Rand{
		rander: rand.New(rand.NewChaCha8(seed)),
	}, nil
}

func MustNewCrypto() Rander {
	var seed [32]byte
	crand.Read(seed[:])

	return &Rand{
		rander: rand.New(rand.NewChaCha8(seed)),
	}
}

func (r *Rand) Int() int {
	return r.rander.Int()
}

func (r *Rand) IntN(n int) int {
	return r.rander.IntN(n)
}

func (r *Rand) Int32() int32 {
	return r.rander.Int32()
}

func (r *Rand) Int32N(n int32) int32 {
	return r.rander.Int32N(n)
}

func (r *Rand) Int64() int64 {
	return r.rander.Int64()
}

func (r *Rand) Int64N(n int64) int64 {
	return r.rander.Int64N(n)
}

func (r *Rand) UintN(n uint) uint {
	return r.rander.UintN(n)
}

func (r *Rand) Uint32() uint32 {
	return r.rander.Uint32()
}

func (r *Rand) Uint32N(n uint32) uint32 {
	return r.rander.Uint32N(n)
}

func (r *Rand) Uint64() uint64 {
	return r.rander.Uint64()
}

func (r *Rand) Uint64N(n uint64) uint64 {
	return r.rander.Uint64N(n)
}

func (r *Rand) ExpFloat64() float64 {
	return r.rander.ExpFloat64()
}

func (r *Rand) NormFloat64() float64 {
	return r.rander.NormFloat64()
}

func (r *Rand) Float32() float32 {
	return r.rander.Float32()
}

func (r *Rand) Float64() float64 {
	return r.rander.Float64()
}

func (r *Rand) Perm(n int) []int {
	return r.rander.Perm(n)
}

func (r *Rand) Shuffle(n int, swap func(i int, j int)) {
	r.rander.Shuffle(n, swap)
}
